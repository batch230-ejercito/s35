const express = require("express");
// Mongoose is a package that allows creation of  Scemas to model data structures
//also has access to a number of methods for manipulating database
const mongoose = require("mongoose");

const app = express();
const port = 3001;

//[SECTION] MONGODB CONNECTION
// CONNECT TO THE DATABASE BY PASSING IN YOUR CONNECTION STRING , REMEMBER TO REPLACE THE PASSWORD AND DATABASE NAMES WITH ACTUAL VALUES
	//syntax mongoose.connect("url")
mongoose.connect("mongodb+srv://admin:admin@batch230.5tt6rxu.mongodb.net/S35?retryWrites=true&w=majority",
	{
			useNewUrlParser: true,
			useUnifiedTopology: true
	}

)
// Connection to database
// allows to handle erros when the initial connection is stablished 
//Works with the on and once Mongoose methods
let db = mongoose.connection
// if a connection error occured, output in the console
//console.error.bind(console) allows us to print errors in the browser terminal

db.on("error", console.error.bind(console,"console error"));
db.once("open", ()=> console.log("We're connected to the cloud database"));

app.use(express.json());

const taskSchema = new mongoose.Schema( {
	name: String,
	status: {
		type: String,
	default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

app.post("/tasks", (request, response) => {
	// check if there are duplicate tasks

	Task.findOne({name: request.body.name}, (err, result) => {
// if there was found and the document's name matches the information via the client/ Postman
		if(result!=null &&  result.name == request.body.name){
			return response.send("Duplicate task found");
		}
		else{

			let newTask = new Task({
				name: request.body.name
			})
				newTask.save((saveErr, savedTask)=> {
					// if an error is saved in saveErr parameter
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return response.status(201).send("New task created");
					}
				})
		}
		
	})
})


app.get("/tasks", (request, response) => {
	
	Task.find({}, (err, result) => {

		if(err){
			return console.log(err);
		}
		else  {
			return response.status(200).json({
				data: result
			})
		

		}
	})
})



app.listen(port, () =>console.log(`Server is running at port ${port}`));



// Activity:
// 1. Create a User schema.
// 2. Create a User model.
// 3. Create a POST route that will access the "/signup" route that will create a user.
// 4. Process a POST request at the "/signup" route using postman to register a user.
// 5. Create a git repository named S35.
// 6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 7. Add the link in Boodle.





/*/ Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
	- If the user already exists in the database, we return an error
	- If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/


const userSchema = new mongoose.Schema( {
	username: String,
	password: String
	
})



const User = mongoose.model("User", userSchema);

app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username,}, (err, result) => {

	if(result!=null &&  result.username == request.body.username){
			return response.send("Duplicate User found!");
		}


		else{

			let NewUser = new User({
				username: request.body.username,
				password: request.body.password
			})
				NewUser.save((saveErr, savedTask)=> {
				
					if(saveErr){
						return console.error(saveErr);
					}
					else{
						return response.status(201).send("New User Registered");
					}
				})
		}
		
	})
})


// app.get("/signup", (request, response) => {
	
// 	User.find({}, (err, result) => {

// 		if(err){
// 			return console.log(err);
// 		}
// 		else  {
// 			return response.status(200).json({
// 				data: result
// 			})
		

// 		}
// 	})
// })